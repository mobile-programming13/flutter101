import 'package:flutter/material.dart';
import 'package:flutter101/home/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Profile',
      theme: ThemeData(
        primarySwatch: Colors.grey.withOpacity(0.4),
      ),
      home: const LoginPage(),
    );
  }
}
